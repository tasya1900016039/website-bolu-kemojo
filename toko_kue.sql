/*
SQLyog Ultimate v8.82 
MySQL - 5.7.24 : Database - toko_kue
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`toko_kue` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `toko_kue`;

/*Table structure for table `biodata_pengguna` */

DROP TABLE IF EXISTS `biodata_pengguna`;

CREATE TABLE `biodata_pengguna` (
  `id_biodata` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `alamat` text,
  `tgl_lahir` date DEFAULT '0000-00-00',
  `jenis_kelamin` enum('laki-laki','perempuan') DEFAULT 'laki-laki',
  `email` text,
  PRIMARY KEY (`id_biodata`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `biodata_pengguna` */

/*Table structure for table `data_produk` */

DROP TABLE IF EXISTS `data_produk`;

CREATE TABLE `data_produk` (
  `kode_produk` varchar(20) DEFAULT NULL,
  `id_toko` varchar(30) DEFAULT NULL,
  `nama_produk` varchar(30) DEFAULT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT '0',
  `stok` int(11) DEFAULT '0',
  `diskon` int(11) DEFAULT '0',
  `harga_total` int(11) DEFAULT '0',
  `foto_produk` text,
  `status` enum('aktif','nonaktif') DEFAULT 'aktif',
  `approved` enum('approved','notapproved','review') DEFAULT 'review',
  `approved_by` int(11) DEFAULT NULL,
  `tgl_upload` date DEFAULT '0000-00-00',
  `tgl_approve` date DEFAULT '0000-00-00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `data_produk` */

/*Table structure for table `data_toko` */

DROP TABLE IF EXISTS `data_toko`;

CREATE TABLE `data_toko` (
  `id_toko` varchar(30) NOT NULL,
  `nama_toko` varchar(50) DEFAULT NULL,
  `deskripsi` text,
  `id_biodata` varchar(30) DEFAULT NULL,
  `alamat_toko` text,
  `instagram` varchar(30) DEFAULT NULL,
  `no_wa` varchar(20) DEFAULT NULL,
  `no_hp` varchar(20) DEFAULT NULL,
  `twitter` varchar(20) DEFAULT NULL,
  `facebook` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_toko`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `data_toko` */

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `id_biodata` int(11) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `password` text,
  `role` enum('administrator','pegawai','penjual') DEFAULT 'administrator',
  `status` enum('aktif','nonaktif') DEFAULT 'aktif',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `login` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
