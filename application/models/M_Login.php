<?php

class M_Login extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function change($id)
    {
        $this->db->where('id_user', $id);
        $this->db->update('login', ['status' => 'nonaktif']);
    }

    function create()
    {
        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('per'),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),,
            'role' => $this->input->post('role'),
            'status' => $this->input->post('aktif')
        );

        $this->db->insert('login', $data);
    }

    function getId($id)
    {
        echo json_encode($this->db->get_where('data_kepemilikan', ['id_kepemilikan' => $id])->row_array());
    }

    function edit()
    {
        $data = array(
            'username' => $this->input->post('username'),
            'password' => $this->input->post('per'),
            'password' => password_hash($this->input->post('password'), PASSWORD_DEFAULT),,
            'role' => $this->input->post('role'),
            'status' => $this->input->post('aktif')
        );
        $this->db->where('id_user', $this->input->post('id_user'));
        $this->db->update('login', $data);
    }
}
