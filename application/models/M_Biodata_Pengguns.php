<?php
class M_Biodata_Pengguna extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    function read()
    {
        return $this->db->get('biodata_pengguna')->result();
    }

    function create()
    {

        $data = array(
            'nama' => $this->input->post('nama'),
            'no_hp' => $this->input->post('no_hp'),
            'alamat' => $this->input->post('alamat'),
            'tgl_lahir' => $this->input->post('tgl_lahir'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'email' => $this->input->post('email')
        );

        $this->db->insert('biodata_pengguna', $data);
    }

    function getId($id)
    {
        echo json_encode($this->db->get_where('biodata_pengguna', ['id_biodata' => $id])->row_array());
    }

    function edit()
    {
        $data = [
            'nama' => $this->input->post('nama'),
            'no_hp' => $this->input->post('no_hp'),
            'alamat' => $this->input->post('alamat'),
            'tgl_lahir' => $this->input->post('tgl_lahir'),
            'jenis_kelamin' => $this->input->post('jenis_kelamin'),
            'email' => $this->input->post('email')
        ];
        $this->db->where('id_biodata', $this->input->post('id_biodata'));
        $this->db->update('biodata_pengguna', $data);
    }

    function delete($id)
    {
        $data = $this->db->get_where('biodata_pengguna', ['id_biodata' => $id])->row_array();
        $this->db->delete('biodata_pengguna', $data);
    }
}
