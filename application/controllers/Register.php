<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Register extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_Biodata_Pengguna');
    }

    public function index()
    {
        $data['biodata_pengguna'] = $this->M_Biodata_Pengguna->read();
    }

    function create()
    {
        $this->M_Biodata_Pengguna->create();
    }

    function getId()
    {
        $this->M_Biodata_Pengguna->getId($_POST['id']);
    }

    function edit()
    {
        $this->M_Biodata_Pengguna->edit($_POST['id']);
    }
    function delete($id)
    {
        $this->M_Biodata_Pengguna->delete($id);
    }
}
