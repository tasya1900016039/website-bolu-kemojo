<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_Login');
        $this->load->model('M_Biodata_Pengguna');
    }

    public function index()
    {
        $this->auth();
    }

    function createLogin()
    {
        $this->M_Login->create();
    }

    function getId()
    {
        $this->M_Login->getId($_POST['id']);
    }

    function edit()
    {
        $this->M_Login->edit($_POST['id']);
    }

    function changeStatus()
    {
        $this->M_Login->change($_POST['id']);
    }

    public function auth()
    {

        $username = $this->input->post('username'); //inputan di halaman login
        $password = $this->input->post('password'); //inputan di halaman login

        $user = $this->db->get_where('login', ['username' => $username])->row_array(); // ambil data inputan login
        // jika user ada
        if ($user) {
            // cek password
            if (password_verify($password, $user['password'])) {
                $data = [
                    'username' => $user['username'],
                    'role' => $user['role'],
                    'loged_in' => TRUE
                ];
                $this->session->set_userdata($data); //simpan data kedalam session
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('role');
    }
}
